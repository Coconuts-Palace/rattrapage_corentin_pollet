package com.pollet.chat;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Chat extends AppCompatActivity {

    private Socket socket              = null;
    private DataOutputStream streamOut = null;
    private DataInputStream streamIn = null;

    TextView tv_chat;
    EditText et_chat;
    Button b_send;

    Boolean isConnected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Intent intent = getIntent();
        String ip = intent.getStringExtra("ip");
        int port  = Integer.parseInt(intent.getStringExtra("port"));
        String user = intent.getStringExtra("user");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        tv_chat = (TextView) findViewById(R.id.chat);
        et_chat = (EditText) findViewById(R.id.text);
        b_send = (Button) findViewById(R.id.send);

        tv_chat.setMovementMethod(new ScrollingMovementMethod());


        et_chat.setEnabled(false);
        b_send.setEnabled(false);


        //tv_chat.append(ip + ":" + port);
        tv_chat.append("Tentative de connection. Veuillez patienter ...\n");
        try {
            socket = new Socket(ip, port);
            tv_chat.append("Connecté ! \n");
            et_chat.setEnabled(true);
            b_send.setEnabled(true);
            isConnected = true;
            start();
            try {
                streamOut.writeUTF("User :" + user);
                streamOut.flush();
            } catch (IOException ioe) {
                tv_chat.append("Erreur de l'envoi: " + ioe.getMessage() + "\n");
            }

        } catch (UnknownHostException uhe) {
            tv_chat.append("Serveur inconnu: " + uhe.getMessage() + "\n");
            isConnected = false;
        } catch (IOException ioe) {
            tv_chat.append("Erreur : " + ioe.getMessage() + "\n");
            isConnected = false;
        }


        b_send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try
                {
                    streamOut.writeUTF(et_chat.getText().toString());
                    et_chat.setText("");
                    streamOut.flush();
                }
                catch(IOException ioe)
                {
                    tv_chat.append("Erreur de l'envoi: " + ioe.getMessage() + "\n");
                }

            }
        });
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Etes vous sûr de vouloir quitter ?")
                    .setCancelable(false)
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try
                            {
                                try
                                {
                                    streamOut.writeUTF(".bye");
                                    streamOut.flush();
                                }
                                catch(IOException ioe)
                                {
                                    tv_chat.append("Erreur de l'envoi: " + ioe.getMessage() + "\n");
                                }

                                if (streamOut != null)  streamOut.close();
                                if (streamIn != null)  streamIn.close();
                                if (socket    != null)  socket.close();
                                isConnected = false;

                                et_chat.setEnabled(false);
                                b_send.setEnabled(false);
                                tv_chat.append("Déconnecté" + "\n");
                                finish();
                            }
                            catch(IOException ioe)
                            {
                                tv_chat.append("Erreur de fermeture ..." + "\n");
                            }

                        }
                    })
                    .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }
        else
        {
            return super.onKeyDown(keyCode, event);
        }
    }



    /*
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK) {


    }
*/
    public void start() throws IOException
    {
        streamOut = new DataOutputStream(socket.getOutputStream());
        streamIn = new DataInputStream(socket.getInputStream());
        Receive r= new Receive();
        new Thread(r).start();

    }
    public class Receive extends Thread {

        public void run() {
            while(isConnected){
                try
                {
                    tv_chat.append(streamIn.readUTF().toString() + "\n");
                }
                catch(Exception e)
                {

                }
            }

        }
    }
}
