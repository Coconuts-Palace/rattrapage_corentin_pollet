package com.pollet.chat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CustomSever extends AppCompatActivity {
    EditText et_address;
    EditText et_port;
    EditText et_username;
    Button b_connect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_sever);


        et_address = (EditText) findViewById(R.id.address);
        et_port = (EditText) findViewById(R.id.port);
        et_username = (EditText) findViewById(R.id.username);
        b_connect = (Button) findViewById(R.id.connect);


        b_connect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if(!et_username.getText().toString().equals("") && !et_address.getText().toString().equals("") && !et_port.getText().toString().equals("")) {
                    Intent myIntent = new Intent(CustomSever.this, Chat.class);
                    myIntent.putExtra("ip", et_address.getText().toString());
                    myIntent.putExtra("port", et_port.getText().toString());
                    myIntent.putExtra("user", et_username.getText().toString());
                    CustomSever.this.startActivity(myIntent);
                }

            }
        });



    }
}
