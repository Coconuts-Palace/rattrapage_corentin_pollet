package com.pollet.chat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class CentralServer extends AppCompatActivity {


    EditText et_username;
    Button b_connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central_server);


        et_username = (EditText) findViewById(R.id.username);
        b_connect = (Button) findViewById(R.id.connect);


        b_connect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if(!et_username.getText().toString().equals("")) {
                    Intent myIntent = new Intent(CentralServer.this, Chat.class);
                    myIntent.putExtra("ip", "31.37.171.46");
                    myIntent.putExtra("port", "2428");
                    myIntent.putExtra("user", et_username.getText().toString());
                    CentralServer.this.startActivity(myIntent);
                }

            }
        });

    }
}
