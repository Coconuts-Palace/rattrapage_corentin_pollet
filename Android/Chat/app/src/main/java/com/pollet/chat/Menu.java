package com.pollet.chat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import java.io.IOException;

public class Menu extends AppCompatActivity {

    Button b_central;
    Button b_custom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        b_central = (Button) findViewById(R.id.serverCentral);
        b_custom = (Button) findViewById(R.id.customServer);

        b_central.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent central = new Intent(Menu.this, CentralServer.class);
                Menu.this.startActivity(central);

            }
        });

        b_custom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent custom = new Intent(Menu.this, CustomSever.class);
                Menu.this.startActivity(custom);

            }
        });
    }


}
